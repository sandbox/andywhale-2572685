/**
 * @file
 * Provides the drag & drop functionality for slices.
 *
 * This enables field instances to move slices into active regions
 * and to mark slices as active / inactive.
 */

(function ($) {
  "use strict";

  Drupal.behaviors.slices = {

    draggedElement: '',

    attach: function (context, settings) {

      $(".field-add-more-submit").not('.slice-processed').on('mousedown', $.proxy(function (e) {
        this.addFieldRow(e.currentTarget);
      }, this)).addClass('slice-processed');

      $(".remove-block-button").not('.slice-processed').on('mousedown', $.proxy(function (e) {
        this.removeFieldRow(e.currentTarget);
      }, this)).addClass('slice-processed');

      $(".block-region-select").not('.slice-processed').on('change', $.proxy(function (e) {
        var $regionSelect = $(e.currentTarget);
        this.updateTitleVisibility($regionSelect.closest('tr'), $regionSelect.val());
      }, this)).addClass('slice-processed');

      $('#blocks .draggable .handle').not('.slice-processed').on('mousedown', $.proxy(function (e) {
        this.draggedElement = $(e.currentTarget).closest('tr');
      }, this)).addClass('slice-processed');

      $('#blocks').not('.slice-processed').on('mouseup', $.proxy(function (e) {
        if (this.draggedElement !== '') {
          $.each($(this.draggedElement).prevAll('.region'), $.proxy(function (index, region) {
            this.dragUpdateTitleVisibility($(this.draggedElement), $(region));
            return false;
          }, this));
        }
      }, this)).addClass('slice-processed');

      $(".slice-title-modify").not('.slice-processed').on('change', $.proxy(function (e) {
        this.changeBlockTitle($(e.currentTarget));
      }, this)).addClass('slice-processed');

    },

    addFieldRow: function (element) {

      var elementId = $(element).attr('id').split('--');
      var field = elementId[0].replace('edit-', '').replace('-und-add-more', '');

      var newIndex = $('[id^=' + field + '-values] > tbody > tr').length + 1;

      var $fieldRow = $('.slice-' + field + '-' + newIndex);

      $fieldRow.removeClass('nlb-hidden');
      $fieldRow.find('.block-region-select').val('content').change();

    },

    removeFieldRow: function (element) {

      var field = $(element).attr('id').replace('edit-', '').replace('-remove-button', '');
      var elementId = field.split('-und-');
      var blockIndex = this.getBlockIndex(elementId[1], 1);
      var fieldName = elementId[0];

      var lastIndex = $('[id^=' + fieldName + '-values] > tbody > tr').length;

      var $fieldRow = $('.slice-' + fieldName + '-' + lastIndex);

      // Alter the weights for the current reorder.
      for (var i = lastIndex - 1; i >= blockIndex; i--) {
        var j = i + 1;
        var prevWeight = $('.slice-' + fieldName + '-' + j).find('.block-weight').val();
        $('.slice-' + fieldName + '-' + i).find('.block-weight').val(prevWeight).change();
        $('.slice-' + fieldName + '-' + i).find('.block-region-select').change();
      }

      $fieldRow.addClass('nlb-hidden');
      $fieldRow.find('.block-region-select').val('disabled').change();

    },

    getBlockIndex: function (blockIndexValue, defaultBlockIndex) {

      if (blockIndexValue) {
        return parseInt(blockIndexValue) + 1;
      }
      else {
        return defaultBlockIndex;
      }

    },

    updateTitleVisibility: function ($regionSelector, regionValue) {

      $.each($regionSelector.closest('tr').attr('class').split(' '), $.proxy(function (i, regionClass) {
        if (regionClass.match(/slice-.*/i)) {
          if (regionValue === -1) {
            $('.' + regionClass.replace('slice-', 'slicetitle-')).addClass('nlb-hidden');
            this.updateActiveStateIcon($('.' + regionClass.replace('slice-', 'sliceicon-')), 'on', 'off');
          }
          else {
            $('.' + regionClass.replace('slice-', 'slicetitle-')).removeClass('nlb-hidden');
            this.updateActiveStateIcon($('.' + regionClass.replace('slice-', 'sliceicon-')), 'off', 'on');
          }
        }
      }, this));

    },

    updateActiveStateIcon: function ($icon, oldstate, newstate) {

      if ($icon.length > 0) {
        var new_icon = $icon.attr('src').replace(oldstate, newstate);
        $icon.attr('src', new_icon);
      }

    },

    dragUpdateTitleVisibility: function ($draggedRow, $regionRow) {

      var regionAssigned = '';

      $.each($regionRow.attr('class').split(' '), function (i, regionClass) {
        if (regionClass.match(/region-.*/i)) {
          regionAssigned = regionClass.replace('region-', '');
        }
      });

      this.updateTitleVisibility($draggedRow, regionAssigned);

    },

    changeBlockTitle: function ($titleField) {

      $.each($titleField.attr('class').split(' '), function (i, regionClass) {
        if (regionClass.match(/slice-title-field-.*/i)) {
          var fieldName = regionClass.replace('slice-title-field-', '');
          $('.slice-title-' + fieldName).text($titleField.val());
          $('.slice-' + fieldName + ' .title').text('Slice: ' + $titleField.val());
        }
      });

    }

  };

})(jQuery);
