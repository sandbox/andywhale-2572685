/**
 * @file
 * Handles the JS for the slices configuration screen.
 *
 * This is purely for handling slice removal confirmation.
 */

(function ($) {
  "use strict";

  Drupal.behaviors.slices = {

    attach: function (context, settings) {

      $('.slice-delete').not('.slice-processed').on('click', function (e) {
        e.preventDefault();
        $(e.currentTarget).hide().closest('tr').slideDown().after('<tr><td colspan="5"><b style="color: #CC0000;">Are you sure you wish to delete this slice? <a class="delete-confirm" href="' + $(e.currentTarget).attr('href') + '">Confirm</a></b></td></tr>');
      }).addClass('slice-processed');

      $('#edit-flex-field').not('.slice-processed').on('change', function (e) {
        if ($(e.currentTarget).find(':selected').text().match(/\*\*.*/i)) {
          $(e.currentTarget).closest('.form-item').find('.description').html("<b style='color: #CC0000;'>This will alter the number of blocks and may result in blocks being removed - remember: with great power comes great responsibility</b>");
        }
        else {
          $(e.currentTarget).closest('.form-item').find('.description').html("The ID of the field to be added (** Fields in use)");
        }
      }).addClass('slice-processed');

    }

  };

})(jQuery);
