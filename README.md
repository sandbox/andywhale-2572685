# Drupal Module: Slices #
## by Numiko ##

## Requirements ##

* Drupal 7.x
* [Node Level Blocks](https://www.drupal.org/project/node_level_blocks)

## Usage ##

Slices are essentially a mapping of a field to a block (or multiple blocks),
these mappings are stored within a slices variable as a serialised array -
indexed by delta, and containing; field, label and where available render
plugins (covered later in this document).

### Multiple Fields ###

When setting up a field to slice mapping one of the properties is to select
the number of slices, if you select 'not multi-field' then however many values
are entered into a field they will be output together as a single block,
however selecting any number of values here [maximum available is set in the
MAX_BLOCKS_PER_FIELD constant] will set a maximum on the number of values you
can add (irrespective of setting a field to unlimited values). This facility
will allow for the organising of field blocks separately in the node level
blocks screen.

### Extending ###

The default functionality of a slice is to simply render the field (or field
collection) attached via the display suite settings, in order to override this
render functionality you would need to add a render plugin (an additional
module). A render plugin needs to contain the following hooks:
(Examples of these are available within Bournemouth Uni Taxonomy Slice)

#### hook_slices_plugins_alter ####

Alter the list of available plugins, by adding to the plugin list, for example

```
/**
 * The plugin alter allows us to add our taxonomy slice to 
 * the plugin select list
 */
function taxonomy_slices_slices_plugins_alter(&$plugin_list) {

  $plugin_list['taxonomy_slices'] = "Taxonomy Slice";

}
```

#### hook_slices_render ####

Creates the output of the slice, this would override the normal rendering of 
the field, but would have all of the field details available to it.

## Outstanding ##

* Tests still required for this module
