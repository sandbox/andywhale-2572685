<?php

/**
 * @file
 * Handles the slice configuration.
 *
 * Handles the display of the slice configuration form, allowing new slices to
 * be created and slices to be removed.
 */

/**
 * This is the slices config form (admin/config/content/slices).
 */
function slices_admin_form() {

  $form = $fields_active = $options = array();

  drupal_add_js(drupal_get_path('module', 'slices') . '/js/slices_admin.js');

  $field_blocks = _slices_get_fields();
  foreach ($field_blocks as $detail) {
    $fields_active[] = $detail['field'];
  }

  $form['flex_existing'] = array(
    '#type' => "fieldset",
    '#title' => "Existing Slices",
  );

  $form['flex_existing']['flex_existing_blocks'] = array(
    '#type' => 'item',
    '#markup' => _slices_existing_table($field_blocks),
  );

  $fields = field_info_fields();
  foreach ($fields as $field_id => $field_detail) {
    // We are only interested in fields used on nodes not already added as a
    // field block.
    if (isset($field_detail['bundles']['node'])) {
      if (!in_array($field_id, $fields_active)) {
        $options[$field_id] = $field_id . ' (' . $field_detail['type'] . ')';
      }
      else {
        $options[$field_id] = '** ' . $field_id . ' (' .
          $field_detail['type'] . ')';
      }
    }
  }

  $form['flex_new'] = array(
    '#type' => "fieldset",
    '#title' => "Add a New Slice",
  );

  $form['flex_new']['flex_field'] = array(
    '#type' => 'select',
    '#title' => t('Field ID'),
    '#empty_option' => '- please select -',
    '#default_value' => '',
    '#options' => $options,
    '#maxlength' => 120,
    '#description' => t("The ID of the field to be added (** Fields in use)"),
    '#required' => TRUE,
  );
  $form['flex_new']['flex_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Block Label'),
    '#default_value' => '',
    '#size' => 60,
    '#maxlength' => 120,
    '#description' => t("The name of the slice to be added"),
    '#required' => TRUE,
  );
  $options = array();
  // Create an array of options.
  for ($i = 1; $i <= SLICES_MAX_BLOCKS_PER_FIELD; $i++) {
    $options[$i] = $i;
  }
  $form['flex_new']['flex_no'] = array(
    '#type' => 'select',
    '#title' => t('Number of blocks'),
    '#empty_option' => '- not multi-field -',
    '#empty_value' => 0,
    '#options' => $options,
    '#description' => t("The number of slices to be added if this is a multi field (e.g. field collection)"),
    '#required' => FALSE,
  );

  $plugins = array();
  drupal_alter('slices_plugins', $plugins);

  $form['flex_new']['flex_plugin'] = array(
    '#type' => 'select',
    '#title' => t('Plugin'),
    '#empty_option' => '- slice output -',
    '#empty_value' => '',
    '#options' => $plugins,
    '#description' => t("Any plugin to be used to display this slice"),
    '#required' => FALSE,
  );

  $form['#submit'][] = 'slices_form_submit';

  return system_settings_form($form);

}

/**
 * Create a table of existing fields.
 */
function _slices_existing_table($field_blocks) {

  $vars = array();

  $vars['header'] = array(
    'Slice Name',
    'Field ID',
    'Slice Delta',
    'Max Slices',
    'Actions',
  );

  $rows = array();
  foreach ($field_blocks as $block_delta => $block_detail) {
    $link = "<a class='slice-delete' href='" . '/slice/' .
      $block_delta . '/delete' . "'>Delete</a>";
    $block_index = (isset($block_detail['max_slices'])
      && $block_detail['max_slices']) ? $block_detail['max_slices'] : 'n/a';
    $rows[] = array(
      $block_detail['label'],
      $block_detail['field'],
      $block_delta,
      $block_index,
      $link,
    );
  }

  $vars['rows'] = $rows;

  $vars['attributes']['classes'][] = "flex-existing";
  $vars['caption'] = "";
  $vars['colgroups'] = array();
  $vars['sticky'] = TRUE;
  $vars['empty'] = "No Slices have been setup";

  return theme('table', $vars);

}

/**
 * Slices form submit handler.
 *
 * We will use the submit callback to serialise all of the output
 * (and append to any existing field blocks).
 */
function slices_form_submit($form, &$form_state) {

  $field_blocks = _slices_get_fields();

  $form_state['values']['prev_slices'] = serialize($field_blocks);

  $block_delta = SLICES_BLOCK_PREFIX .
    str_replace('field_', '', $form_state['values']['flex_field']);

  $max_slices = ($form_state['values']['flex_no'])
    ? $form_state['values']['flex_no'] : 0;

  $field_blocks[$block_delta] = array(
    'field' => $form_state['values']['flex_field'],
    'label' => $form_state['values']['flex_label'],
    'max_slices' => $max_slices,
    'render_plugin' => $form_state['values']['flex_plugin'],
  );

  unset($form_state['values']['flex_field']);
  unset($form_state['values']['flex_label']);
  unset($form_state['values']['flex_no']);
  unset($form_state['values']['flex_plugin']);

  $form_state['values']['slices'] = serialize($field_blocks);

}

/**
 * Menu callback to remove a selected field block.
 */
function slices_remove_block($block_delta) {

  $field_blocks = _slices_get_fields();

  if (isset($field_blocks[$block_delta])) {
    unset($field_blocks[$block_delta]);
  }

  variable_set('slices', serialize($field_blocks));

  drupal_goto('admin/config/content/slices');

}
